const path = require("path");
const fs = require("fs");

let fileName = process.env.FILE_NAME
let pathName = process.env.PATH_NAME

const { GoogleDriveService } = require("./gdrive/utils");

const bufferCredential = fs.readFileSync(path.resolve(__dirname, "gdrive/credentials.json"));
const credential = JSON.parse(bufferCredential.toString());

const bufferAccess = fs.readFileSync(path.resolve(__dirname, "gdrive/token.json"));
const access = JSON.parse(bufferAccess.toString());

const driveClientId = credential.installed.client_id || "";
const driveClientSecret = credential.installed.client_secret || "";
const driveRedirectUri = credential.installed.redirect_uris || "";
const driveToken = access || "";

(async () => {
  const googleDriveService = new GoogleDriveService(
    driveClientId,
    driveClientSecret,
    driveRedirectUri,
    driveToken
  );

  const finalPath = path.resolve(pathName, fileName);
  const folderName = "backup";

  if (!fs.existsSync(finalPath)) {
    throw new Error("File not found!");
  }

  let folder = await googleDriveService
    .searchFolder(folderName)
    .catch((error) => {
      console.error(error);
      return null;
    });

  if (!folder) {
    folder = await googleDriveService.createFolder(folderName);
  }

  await googleDriveService
    .saveFile(path.basename(finalPath), finalPath, "image/jpg", folder.id)
    .catch((error) => {
      console.error(error);
    });

  console.info("File uploaded successfully!");
})();
