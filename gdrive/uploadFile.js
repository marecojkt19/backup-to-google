const { google, drive_v3 } = require("googleapis");
const fs = require("fs");
var fileMetadata = {
  name: "photo.jpg",
};
var media = {
  mimeType: "image/jpeg",
  body: fs.createReadStream("files/data.jpg"),
};
drive_v3.files.create(
  {
    resource: fileMetadata,
    media: media,
    fields: "id",
  },
  function (err, file) {
    if (err) {
      // Handle error
      console.error(err);
    } else {
      console.log("File Id: ", file.id);
    }
  }
);
